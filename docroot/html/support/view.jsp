<%@page import="java.net.URLEncoder"%>
<%@page import="java.util.List"%>
<%@page import="javax.portlet.PortletSession"%>
<%@page import="com.liferay.portal.model.EmailAddress"%>
<%@page import="com.liferay.portal.model.Role"%>
<%@page import="com.welive.support.util.Constants"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<portlet:actionURL var="technicalSupportURL" windowState="normal" name="technicalSupport">
</portlet:actionURL>

<portlet:actionURL var="backURL" windowState="normal" name="back">
</portlet:actionURL>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
String casLoginUrl = "/c/portal/login";
boolean login = false;
String pilot = (String)portletSession.getAttribute("LIFERAY_SHARED_pilot", PortletSession.APPLICATION_SCOPE);
String fullname = "";
String email = "";
String roleName = "";
boolean isDeveloper = false;

try {
	if(themeDisplay.isSignedIn() && user != null) {
		if(pilot == null) {
			String[] pilotArray = (String [])user.getExpandoBridge().getAttribute("pilot");
			if(pilotArray != null && pilotArray.length > 0) {
				pilot = pilotArray[0];
				portletSession.setAttribute("LIFERAY_SHARED_pilot", pilot);
			}
		}
		fullname = user.getFullName();
		email = user.getDisplayEmailAddress();
		List<Role> roleList = user.getRoles();
		for(Role role : roleList) {
			String roleNameEval = role.getName();
			if(Constants.ROLE_DEVELOPER.equals(roleNameEval)) {
				isDeveloper = true;
			} else {
				for(String roleNameValid : Constants.ROLE_LIST) {
					if(roleNameValid.equals(roleNameEval)) {
						roleName = roleNameValid;
						break;
					}
				}
			}
		}
	} else {
		HttpServletRequest httpRequest = PortalUtil.getOriginalServletRequest(request);
		if("true".equals(httpRequest.getParameter("login"))) {
			login = true;
		}
	}
} catch(Exception e) {}
%>

<%-- Overridden font (HTTPS reference needed) --%>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<c:choose>
	
	<c:when test="<%=login%>">
		<script>
			location.replace("<%=casLoginUrl%>");
		</script>
	</c:when>
	<c:otherwise>

		<div id="materialize-body" class="materialize">
			<form action="<%=technicalSupportURL%>" id="form_technical" method="POST">
				<div class="section">
					<h4><liferay-ui:message key="support.title" /></h4>
					<p><liferay-ui:message key="support.form.intro" /></p>
					<div class="row">
						<div class="input-field col s12 m12 l6">
							<input id="name" name="<portlet:namespace/>name" type="text" required="true" class="validate" value="<%=fullname%>" autocomplete="off">
							<label for="name"><liferay-ui:message key="support.form.fullname" /></label>
						</div>
						<div class="input-field col s12 m12 l6">
							<input id="email" name="<portlet:namespace/>email" type="email" required="true" class="validate" value="<%=email%>" autocomplete="off">
							<label for="email" data-error=""><liferay-ui:message key="support.form.email" /></label>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m12 l6">
							<label for="user_type"><liferay-ui:message key="support.form.user_type" /></label>
							<select name="<portlet:namespace/>user_type">
								<option value="Citizen"  <%=Constants.ROLE_CITIZEN.equals(roleName)?"selected":""%>><liferay-ui:message key="support.form.user_type.citizen" /></option>
								<option value="Business"  <%=Constants.ROLE_BUSINESS.equals(roleName)?"selected":""%>><liferay-ui:message key="support.form.user_type.company" /></option>
								<option value="Administration"  <%=Constants.ROLE_ACADEMY.equals(roleName)?"selected":""%>><liferay-ui:message key="support.form.user_type.administration" /></option>
								<option value="Entrepeneure"  <%=Constants.ROLE_ENTREPRENEURE.equals(roleName)?"selected":""%>><liferay-ui:message key="support.form.user_type.entrepeneure" /></option>
								<option value="Developer" <%=isDeveloper?"selected":""%>><liferay-ui:message key="support.form.user_type.developer" /></option>
							</select>
						</div>
						<div class="col s12 m12 l6">
							<label><liferay-ui:message key="support.form.pilot" /></label>
							<select name="<portlet:namespace/>pilot">
								<option value="<%=Constants.PILOT_CITY_BILBAO%>" <%=Constants.PILOT_CITY_BILBAO.equals(pilot)?"selected":""%>>Bilbao</option>
								<option value="<%=Constants.PILOT_CITY_HELSINKI%>" <%=Constants.PILOT_CITY_HELSINKI.equals(pilot)?"selected":""%>>Helsinki-Uusimaa</option>
								<option value="<%=Constants.PILOT_CITY_NOVISAD%>" <%=Constants.PILOT_CITY_NOVISAD.equals(pilot)?"selected":""%>>Novi Sad</option>
								<option value="<%=Constants.PILOT_CITY_TRENTO%>" <%=Constants.PILOT_CITY_TRENTO.equals(pilot)?"selected":""%>>Trento</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m12 l6">
							<label><liferay-ui:message key="support.form.request_type" /></label>
							<select name="<portlet:namespace/>request_type">
								<option value="type_general" ><liferay-ui:message key="support.form.request_type.general" /></option>
								<option value="type_technical"><liferay-ui:message key="support.form.request_type.technical" /></option>
								<option value="type_info"><liferay-ui:message key="support.form.request_type.info" /></option>
					
							</select>							
						</div>
						
						
					
					</div>
					<div class="row">
						<div class="input-field col s12 m12 l12">
							<textarea id="text" name="<portlet:namespace/>text" required="true" class="materialize-textarea" length="140"></textarea>
							<label for="text"><liferay-ui:message key="support.form.message" /></label>
						</div>
					</div>
				</div>
				<div class="section">
					
					<a class="red lighten-1 white-text btn btn-flat" href="<%=backURL%>"><liferay-ui:message key="support.back" /></a>
					<button type="submit" class="green lighten-1 white-text btn btn-flat"><liferay-ui:message key="support.send" /></button>					
				</div>
			</form>
		</div>
		
		<script>
			(function($) {
				$(function() {
		
				}); // end of document ready
		
				$(window).load(function() {
					$('.hiddendiv').hide();
				}); // end of window load
		
			})(jQuery); // end of jQuery name space
		</script>
	
	</c:otherwise>
</c:choose>
