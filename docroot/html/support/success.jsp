<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.model.EmailAddress"%>
<%@page import="com.liferay.portal.model.Role"%>
<%@page import="com.welive.support.util.Constants"%>
<%@taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:actionURL var="backURL" windowState="normal" name="back">
</portlet:actionURL>

<%-- Overridden font (HTTPS reference needed) --%>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<div id="materialize-body" class="materialize">
	<div class="section">
		<a class="red lighten-1 white-text btn btn-flat" href="<%=backURL%>"><liferay-ui:message key="support.back" /></a>
	</div>
</div>
