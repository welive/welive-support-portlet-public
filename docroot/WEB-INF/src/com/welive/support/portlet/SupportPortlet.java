package com.welive.support.portlet;

import java.io.IOException;

import javax.mail.internet.InternetAddress;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.liferay.util.mail.MailEngine;
import com.welive.support.logging.Logger;
import com.welive.support.util.PropertyGetter;

/**
 * Portlet implementation class SupportPortlet
 */
public class SupportPortlet extends MVCPortlet {
	
	private static final Log _log = LogFactoryUtil.getLog(SupportPortlet.class.getName());
	
	public static final String KEY_BACK = "back";
	
	private String mailReceiver;

	@Override
	public void init() throws PortletException {
		super.init();
		
		PropertyGetter propertyGetter = PropertyGetter.getInstance();
		try {
			mailReceiver = PrefsPropsUtil.getString("welive.mail.receiver");
		} catch (SystemException e) {
			mailReceiver = propertyGetter.getProperty("welive.mail.receiver");
		}
	}

	
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		HttpServletRequest httpRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));
		String pageBack = httpRequest.getParameter(KEY_BACK);
		
		if (pageBack != null) {
			PortletSession session = renderRequest.getPortletSession();
			session.setAttribute(KEY_BACK, pageBack);
		}
		
		super.doView(renderRequest, renderResponse);
		
//		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	}
	
	
	public void technicalSupport(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		String name = ParamUtil.getString(actionRequest, "name", null);
		String email = ParamUtil.getString(actionRequest, "email", null);
		String userType = ParamUtil.getString(actionRequest, "user_type", null);
		String pilot = ParamUtil.getString(actionRequest, "pilot", null);
		String requestType = ParamUtil.getString(actionRequest, "request_type", null);
		String text = ParamUtil.getString(actionRequest, "text", null);
		
		String mailReceiver;
		try {
			mailReceiver = PrefsPropsUtil.getString(String.format("welive.mail.receiver.%s", pilot), this.mailReceiver);
		} catch (SystemException e) {
			mailReceiver = this.mailReceiver;
		}
		
		StringBuffer messageBuffer = new StringBuffer();
		messageBuffer.append(String.format("name: %s\n", name));
		messageBuffer.append(String.format("email: %s\n", email));
		messageBuffer.append(String.format("user_type: %s\n", userType));
		messageBuffer.append(String.format("pilot: %s\n", pilot));
		messageBuffer.append(String.format("request_type: %s\n", requestType));
		messageBuffer.append(String.format("text: %s\n", text));
		
		MailMessage mailMessage = new MailMessage();
		mailMessage.setHTMLFormat(false);
		mailMessage.setSubject("WeLive Support");
		mailMessage.setBody(messageBuffer.toString());
		try {
//			if(this.mailReceiver.equals(mailReceiver)) {
//				mailMessage.setFrom(new InternetAddress(email, name));
//			} else {
//				mailMessage.setFrom(new InternetAddress(this.mailReceiver, this.mailReceiver));
//			}
			//mailMessage.setFrom(new InternetAddress(email, name));
			mailMessage.setFrom(new InternetAddress(mailReceiver));
			mailMessage.setTo(new InternetAddress(mailReceiver));
			MailEngine.send(mailMessage);
//			MailServiceUtil.sendEmail(mailMessage);
			boolean result = Logger.getInstance().postContactForm();
//			SessionMessages.add(actionRequest, "request_processed", "support.success");
			actionResponse.setRenderParameter("mvcPath", "/html/support/success.jsp");
		} catch (Exception e) {
			SessionErrors.add(actionRequest, "error-key");
			_log.error(e);
		}
	}
	
	public void back(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		PortletSession session = actionRequest.getPortletSession();
		String pageBack = (String)session.getAttribute(KEY_BACK);
		if(pageBack == null) {
			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			pageBack = themeDisplay.getURLHome();
		}
		
		SessionMessages.add(actionRequest, getPortletConfig().getPortletName() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_SUCCESS_MESSAGE);
		actionResponse.sendRedirect(pageBack);
	}
}
