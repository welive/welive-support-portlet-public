package com.welive.support.logging;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.UriBuilder;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.welive.support.util.Constants;
import com.welive.support.util.PropertyGetter;

public class Logger {
	private static final Log _log = LogFactoryUtil.getLog(Logger.class.getName());

	public final static String PILOT_CITY_TRENTO = "Trento";
	public final static String PILOT_CITY_BILBAO = "Bilbao";
	public final static String PILOT_CITY_NOVISAD = "Novisad";
	public final static String PILOT_CITY_HELSINKI = "Uusimaa";
	public final static String PILOT_CITY_LIST[] = {PILOT_CITY_TRENTO, PILOT_CITY_BILBAO, PILOT_CITY_NOVISAD, PILOT_CITY_HELSINKI};
	
	private final static Map<String, String> PILOT_CITY_MAP;
	static {
        Map<String, String> map = new HashMap<String, String>(PILOT_CITY_LIST.length);
        map.put(Constants.PILOT_CITY_TRENTO, PILOT_CITY_TRENTO);
        map.put(Constants.PILOT_CITY_BILBAO, PILOT_CITY_BILBAO);
        map.put(Constants.PILOT_CITY_NOVISAD, PILOT_CITY_NOVISAD);
        map.put(Constants.PILOT_CITY_HELSINKI, PILOT_CITY_HELSINKI);
        PILOT_CITY_MAP = Collections.unmodifiableMap(map);
    }
	
	boolean async;
	private WebResource webResource;
	private String appId;
	
	private static Logger instance;
	
	public interface AsyncLoggerReceiver {
		public void receiveResult(boolean result);
	}
	
	private class AsyncLogger extends Thread {
		private AsyncLoggerReceiver receiver;
		private String type;
		private String message;
		private JSONObject customAttr;
		
		public AsyncLogger(String type, String message, JSONObject customAttr) {
			this(null, type, message, customAttr);
		}
		
		public AsyncLogger(AsyncLoggerReceiver receiver, String type, String message, JSONObject customAttr) {
			this.receiver = receiver;
			this.type = type;
			this.message = message;
			this.customAttr = customAttr;
		}
		
		@Override
		public void run() {
			boolean result = Logger.this.postSync(type, message, customAttr);
			if(receiver != null) {
				receiver.receiveResult(result);
			}
		}
	}
	
	private Logger() {
		PropertyGetter propertyGetter = PropertyGetter.getInstance();
		String url;
		String username;
		String password;
		try {
			appId = PrefsPropsUtil.getString("welive.controller.appId", propertyGetter.getProperty("welive.controller.appId"));
			url = PrefsPropsUtil.getString("welive.logging.url");
			username = PrefsPropsUtil.getString("welive.username", propertyGetter.getProperty("welive.username"));
			password = PrefsPropsUtil.getString("welive.password", propertyGetter.getProperty("welive.password"));
			String sAsync = PrefsPropsUtil.getString("welive.logging.async", propertyGetter.getProperty("welive.logging.async"));
			async = Boolean.parseBoolean(sAsync);
		} catch (SystemException e) {
			appId = propertyGetter.getProperty("welive.controller.appId");
			url = propertyGetter.getProperty("welive.logging.url");
			username = propertyGetter.getProperty("welive.username");
			password = propertyGetter.getProperty("welive.password");
			String sAsync = propertyGetter.getProperty("welive.logging.async");
			async = Boolean.parseBoolean(sAsync);
		}
		url = String.format("%s/%s", url, appId);
		Client client = Client.create();
		client.addFilter(new HTTPBasicAuthFilter(username, password));
		webResource = client.resource(UriBuilder.fromUri(url).build());
	}
	
	public static Logger getInstance() {
		if(instance == null) {
			instance = new Logger();
		}
		return instance;
	}
	
	private boolean post(String type, String message, JSONObject customAttr) {
		if(async) {
			return postAsync(type, message, customAttr);
		} else {
			return postSync(type, message, customAttr);
		}
	}
	
	private boolean postAsync(String type, String message, JSONObject customAttr) {
		(new AsyncLogger(type, message, customAttr)).start();
		return true;
	}
	
	private boolean postSync(String type, String message, JSONObject customAttr) {
		Date date = new Date();

		JSONObject requestJson = JSONFactoryUtil.createJSONObject();
		requestJson.put("appId", appId);
		requestJson.put("timestamp", date.getTime());
		requestJson.put("type", type);
		requestJson.put("message", message);
		requestJson.put("custom_attr", customAttr);

		ClientResponse response = webResource.type("application/json").post(ClientResponse.class, requestJson.toString());

		int responseStatus = response.getStatus();
		if (responseStatus == 200) {
			_log.info(String.format("%s reported successfully.", type));
			return true;
		} else {
			_log.error(String.format("%s ERROR.", type));
			return false;
		}
	}
	
	public boolean postComponentSelected(String componentName) {
		JSONObject customAttr = JSONFactoryUtil.createJSONObject();
		customAttr.put("componentname", componentName);
		return post("ComponentSelected", "ComponentSelected", customAttr);
	}
	
	public boolean postComponentNavigationTime(long navigationSeconds) {
		JSONObject customAttr = JSONFactoryUtil.createJSONObject();
		customAttr.put("navigationseconds", navigationSeconds);
		return post("ComponentNavigationTime", "ComponentNavigationTime", customAttr);
	}
	
	public boolean postCitySelected(String cityName) {
		String name = PILOT_CITY_MAP.get(cityName);
		if(name == null) {
			name = cityName;
		}
		JSONObject customAttr = JSONFactoryUtil.createJSONObject();
		customAttr.put("pilot", name);
		return post("CitySelected", "CitySelected", customAttr);
	}
	
	public boolean postContactForm(String yes) {
		JSONObject customAttr = JSONFactoryUtil.createJSONObject();
		customAttr.put("yes", yes);
		return post("ContactForm", "ContactForm", customAttr);
	}
	
	public boolean postContactForm() {
		return postContactForm("Yes");
	}
}
